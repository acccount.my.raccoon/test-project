<?php

class UserController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpload()
    {
        $model = new User;

        $error = $this->validate();

        if ($error) {
            return $this->render('upload', ['model' => $model, 'error' => $error]);
        }

        if (!empty($_FILES['User']['tmp_name']['csv_file'])) {
            $pathFile = $_FILES['User']['tmp_name']['csv_file'];
            $users = [];

            $fp = fopen($pathFile, 'r');
            while (($data = fgetcsv($fp, 1000, ';')) !== false) {
                $users[] = mb_convert_encoding($data, "UTF-8", "WINDOWS-1251");
            }
            fclose($fp);

            unset($users[0]);
            $statistics = $this->getUserStatistics($users);

            return $this->render('view', ['statistics' => $statistics]);
        }

        return $this->render('upload', ['model' => $model, 'error' => $error]);
    }


    /**
     * This function check database and create User
     *
     * @param array $users the user data from CSV-file
     * @return array
     * @throws Exception
     */
    private function getUserStatistics(array $users): array
    {
        $countNewUsers = 0;
        $updateCount = 0;
        foreach ($users as &$user) {
            $sql = 'SELECT * FROM user as u WHERE u.username = :username AND u.birthday = :birthday';

            $birthday = new DateTimeImmutable($user[3]);
            $user[3] = $birthday->format('Y-m-d');

            $existUser = User::model()->findBySql($sql, ['username' => $user[0], 'birthday' => $birthday->format('Y-m-d')]);

            $currentDate = new DateTimeImmutable('now');

            if (!$existUser) {
                $this->createUser($user, $birthday, $currentDate);

                $countNewUsers++;
            } else {
                $difference = array_diff($existUser->attributes, $user);
                unset($difference['id'], $difference['created_at'], $difference['updated_at']);

                if (!empty($difference)) {
                    $this->updateUser($existUser, $user, $birthday, $currentDate);
                    $updateCount++;
                }
            }
        }

        $deleteCount = $this->getDeleteCount($users);

        return [
            'createCount' => $countNewUsers,
            'updateCount' => $updateCount,
            'deleteCount' => $deleteCount,
        ];

    }

    /**
     * This function create User.
     *
     * @param array             $user        user data from CSV-file
     * @param DateTimeImmutable $birthday    DateTimeImmutable instance
     * @param DateTimeImmutable $currentDate DateTimeImmutable instance
     * @return void
     */
    private function createUser(array $user, DateTimeImmutable $birthday, DateTimeImmutable $currentDate): void
    {
        $model = new User();
        $model->username = $user[0];
        $model->email = $user[1];
        $model->phone = $user[2];
        $model->birthday = $birthday->format('Y-m-d');
        $model->address = $user[4];
        $model->organization_name = $user[5];
        $model->position = $user[6];
        $model->employment_type = $user[7];
        $model->employment_date = $user[8];
        $model->created_at = $currentDate->format('Y-m-d');
        $model->updated_at = $currentDate->format('Y-m-d');

        $model->save();
    }

    /**
     * This function update User.
     *
     * @param User              $user        User object
     * @param array             $data        the data from CSV file
     * @param DateTimeImmutable $birthday    DateTimeImmutable instance
     * @param DateTimeImmutable $currentDate DateTimeImmutable instance
     * @return void
     */
    private function updateUser(User $user, array $data, DateTimeImmutable $birthday, DateTimeImmutable $currentDate)
    {
        $user->username = $data[0];
        $user->email = $data[1];
        $user->phone = $data[2];
        $user->birthday = $birthday->format('Y-m-d');
        $user->address = $data[4];
        $user->organization_name = $data[5];
        $user->position = $data[6];
        $user->employment_type = $data[7];
        $user->employment_date = $data[8];
        $user->updated_at = $currentDate->format('Y-m-d');

        $user->save();
    }

    /**
     * This function delete User and return deleted count.
     *
     * @param array $userCSV the data form CSV-file
     * @return int
     */
    private function getDeleteCount(array $userCSV): int
    {
        $users = User::model()->findAll();

        $usernames = array_column($users, 'username');
        $usernamesCSV = array_column($userCSV,0);

        $deletedCSV = array_diff($usernames,$usernamesCSV);

        $count = 0;
        foreach ($deletedCSV as $user) {
            User::model()->deleteAll('username=:username', [':username' =>$user]);

            $count++;
        }

        return $count;
    }

    /**
     * This function check valid file.
     *
     * @return string
     */
    private function validate(): string
    {

        if (isset($_FILES['User'])) {
            if ($_FILES['User']['type']['csv_file'] !== 'text/csv') {
                return 'Invalid file format. Use a CSV file.';
            }

            if ($_FILES['User']['size']['csv_file'] > 8388608) {
                return 'Upload a file smaller than 1MB.';
            }
        }

        return '';
    }

}
