<?php

class m220714_051524_new_user_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('user', [
            'id' => 'int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'username' => 'varchar(128)',
            'email' => 'varchar(128)',
            'phone' => 'varchar(128)',
            'birthday' => 'date',
            'address' => 'varchar(256)',
            'organization_name' => 'varchar(128)',
            'position' => 'varchar(128)',
            'employment_type' =>'varchar(128)',
            'employment_date' => 'varchar(128)',
            'created_at' => 'date',
            'updated_at' => 'date',
        ]);
	}

	public function down()
	{
		$this->dropTable('user');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}