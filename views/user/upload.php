<?php
/* @var $this UserController */
/* @var $model User */
/* @var $error */

$this->breadcrumbs=array(
	'Upload',
);
?>

<h1>Upload User</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'error' => $error)); ?>