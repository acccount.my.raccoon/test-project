<?php
/* @var $this UserController */
/* @var $statistics */

$this->breadcrumbs=array(
	'Upload',
);
?>

<h1>User Statistics</h1>

<div class="element" >
    <p>Create count = <?php echo  $statistics['createCount']?></p>
    <p>Update count = <?php echo  $statistics['updateCount']?></p>
    <p>Delete count = <?php echo  $statistics['deleteCount']?></p>
</div>

