<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
/* @var $error */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', [
        'id'                   => 'user-form',
        // Please note: When you enable ajax validation, make sure the  corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'htmlOptions'          => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?php echo $form->labelEx($model, 'csv_file'); ?>
    <?php echo $form->fileField($model, 'csv_file'); ?>
    <?php echo $form->error($model, 'csv_file'); ?>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Upload'); ?>
    </div>
    <?php if ($error): ?>
        <div class="errorMessage">
            <?php echo $error; ?>
        </div>
    <?php endif; ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->